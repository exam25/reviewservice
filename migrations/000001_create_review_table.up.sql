CREATE TABLE IF NOT EXISTS rewiews (
    name TEXT,
    post_id INT NOT NULL, 
    customer_id INT NOT NULL, 
    rating INTEGER NOT NULL CHECK(rating >= 1 AND rating <= 5),
    description TEXT NOT NULL, 
    id serial PRIMARY KEY,
    created_at TIME NOT NULL DEFAULT NOW(), 
    updated_at TIME NOT NULL DEFAULT NOW(),
    deleted_at TIME
);
