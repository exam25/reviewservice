package postgres

import (
	"fmt"
	pbr "github/FIrstService/exammicro/reviewservice/genproto/review"
	"log"

	"github.com/jmoiron/sqlx"
)

type reviewRepo struct {
	db *sqlx.DB
}

// NewReviewRepo ...

func NewReviewRepo(db *sqlx.DB) *reviewRepo {
	return &reviewRepo{db: db}
}

func (r *reviewRepo) CreateReview(review *pbr.ReviewReq) (*pbr.ReviewResp, error) {
	reviewResp := &pbr.ReviewResp{}
	fmt.Println(review)
	err := r.db.QueryRow(`insert into rewiews 
	(post_id,customer_id,rating,description,name) 
	values ($1,$2,$3,$4,$5) returning post_id,customer_id,rating,description,name,id`,
		review.PostId, review.CustomerId, review.Rating, review.Description, review.Name).Scan(
		&reviewResp.PostId, &reviewResp.CustomerId, &reviewResp.Rating, &reviewResp.Description, &reviewResp.Name, &reviewResp.Id)
	if err != nil {
		return &pbr.ReviewResp{}, err
	}
	return reviewResp, nil
}

func (r *reviewRepo) GetReviewById(req *pbr.ID) (*pbr.ReviewResp, error) {
	tempReview := &pbr.ReviewResp{}
	err := r.db.QueryRow(`select post_id,customer_id,rating,description,name,id from rewiews where id=$1 and deleted_at is null`, req.Id).Scan(
		&tempReview.PostId, &tempReview.CustomerId, &tempReview.Rating, &tempReview.Description, &tempReview.Name, &tempReview.Id)
	if err != nil {
		log.Fatal("Error while select owners", err)
		return &pbr.ReviewResp{}, err
	}
	return tempReview, nil
}
func (r *reviewRepo) GetByPostId(req *pbr.ID) (*pbr.GetRewiewsRes, error) {
	rows, err := r.db.Query(`select post_id,customer_id,rating,description,name,id 
	from rewiews 
	where post_id=$1 and deleted_at is null`, req.Id)
	if err != nil {
		return &pbr.GetRewiewsRes{}, err
	}
	defer rows.Close()
	response := &pbr.GetRewiewsRes{}

	for rows.Next() {
		temp := &pbr.Review{}
		err = rows.Scan(
			&temp.PostId,
			&temp.CustomerId,
			&temp.Rating,
			&temp.Description,
			&temp.Name,
			&temp.Id,
		)
		if err != nil {
			return &pbr.GetRewiewsRes{}, err
		}
		response.Rewiews = append(response.Rewiews, temp)
	}
	err = r.db.QueryRow(`SELECT ROUND(SUM(rating) / COUNT(post_id)) from rewiews 
	   where post_id = $1 and deleted_at is null 
	   group by id`, req.Id).Scan(&response.Rating)
	if err != nil {
		return &pbr.GetRewiewsRes{}, err
	}
	return response, nil
}

func (r *reviewRepo) UpdateReview(req *pbr.Review) (*pbr.Review, error) {
	rewResp := pbr.Review{}
	err := r.db.QueryRow(`
		UPDATE rewiews SET updated_at=NOW(),post_id=$1, 
		customer_id=$2,
		rating=$3,
		description=$4
		WHERE id=$5 and deleted_at is null returning id,post_id,customer_id,rating,description
	`, req.PostId, req.CustomerId,
		req.Rating, req.Description, req.Id).Scan(
		&rewResp.Id, &rewResp.PostId,
		&rewResp.CustomerId, &rewResp.Rating, &rewResp.Description)
	if err != nil {
		return nil, err
	}
	return req, nil
}

func (r *reviewRepo) DeleteByPostId(req *pbr.ID) error {
	_, err := r.db.Exec(`UPDATE rewiews SET deleted_at=NOW() where post_id=$1 and deleted_at is null`, req.Id)
	return err
}

func (r *reviewRepo) DeleteByCustomerId(req *pbr.ID) error {
	_, err := r.db.Exec(`UPDATE rewiews SET deleted_at=NOW() where customer_id=$1 and deleted_at is null`, req.Id)
	return err
}
