package repo

import pbr "github/FIrstService/exammicro/reviewservice/genproto/review"

// ReviewStorageI ...
type ReviewStorageI interface {
	CreateReview(*pbr.ReviewReq) (*pbr.ReviewResp, error)
	GetReviewById(*pbr.ID) (*pbr.ReviewResp, error)
	UpdateReview(*pbr.Review) (*pbr.Review, error)
	DeleteByPostId(*pbr.ID) error
	DeleteByCustomerId(*pbr.ID) error
	GetByPostId(*pbr.ID) (*pbr.GetRewiewsRes, error)
}
